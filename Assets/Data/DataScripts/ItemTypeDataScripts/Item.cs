﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A scriptable object for in-game item.
/// </summary>
public class Item : ScriptableObject
{
    public string itemName;
    public Sprite icon;

    public virtual void Use()
    {

    }
}
