﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variable/IntVariable", fileName = "NewIntVariable")]
public class IntVariable : ScriptableObject
{
    public int Value;
}
