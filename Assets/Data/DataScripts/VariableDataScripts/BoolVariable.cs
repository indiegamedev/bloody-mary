﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Variable/Bool Variable", fileName = "NewBoolVariable")]
public class BoolVariable : ScriptableObject
{
    public bool Value;
}
