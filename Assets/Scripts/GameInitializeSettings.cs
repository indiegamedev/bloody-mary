﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This scriptable object is a config for the game's initialization.
/// </summary>
[CreateAssetMenu(menuName = "Initialize Settings/Game Initialize Settings", fileName = "GameInitializeSettings")]
public class GameInitializeSettings : ScriptableObject {

	/// <summary>
	/// The version of the application.
	/// </summary>
	[Header("BASIC SETTINGS")] [Space(15)] public string GameVersion = "0.0";

	/// <summary>
	/// Should we log the debugs inside Unity?
	/// </summary>
	[Space] public bool LogDebugs = true;

	/// <summary>
	/// Target Framerate for the application.
	/// </summary>
	[Space(10)] [Range(30, 999)] public int TargetApplicationFPS = 120;
}

