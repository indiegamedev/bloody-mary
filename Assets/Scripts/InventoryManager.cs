﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the game inventory.
/// </summary>
public class InventoryManager : MonoBehaviour
{
    public List<Item> SlotList = new List<Item>();

	/// <summary>
	/// The inventory item parent. If you add a new slot set this as its parent transform.
	/// </summary>
	[Space] public Transform ItemParent;

	/// <summary>
	/// The slot objects in the inventory. Assigned on runtime.
	/// </summary>
	[HideInInspector] public GameObject[] InventorySlotObjects;

    private void Awake ()
    {
		InitializeInventory();
    }

	private void InitializeInventory ()
	{
		InventoryManager mng = AutoMonoBehaviour<InventoryManager>.Instance;

		List<GameObject> _slotList = new List<GameObject>();

		foreach (Transform child in ItemParent.transform)
		{
			_slotList.Add(child.gameObject);
		}

		InventorySlotObjects = _slotList.ToArray();

		UIPlayer.Instance.SetInventoryState(false);
	}

    public void Add (Item item)
    {
        if (SlotList.Count < 20)
        {
            SlotList.Add(item);
        }

        UpdatePanelSlots();
    }

    public void Remove (Item item)
    {
        SlotList.Remove(item);
        UpdatePanelSlots();
    }

    private void UpdatePanelSlots ()
    {
        int _index = 0;
	
        foreach (Transform child in ItemParent.transform)
        {
            InventorySlotController _slot = child.GetComponent<InventorySlotController>();

            _slot.Item = _index < SlotList.Count ? SlotList[_index] : null;
            _slot.UpdateInfo();
            _index++;
        }	
    }
}
