﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

/// <summary>
/// This class is responsible for the player's crouching mechanics.
/// </summary>
[RequireComponent(typeof(CharacterController))] [RequireComponent(typeof(FirstPersonController))] public class CrouchController : MonoBehaviour
{
    public CharacterController CharacterController
	{
		get; private set;
	}

    public FirstPersonController FPSController
	{
		get; private set;
	}

    private bool isCrouching = false;
    private float standingHeight;
    private float crouchHeight;
    private float currentHeight;

    [Tooltip("The speed at which the player transitions from standing to crouching, or vice versa.")]
    [Range(0.1f, 20.0f)]
    public float CrouchTransitionSpeed;

	/// <summary>
	/// Do we want to make crouching togglable?
	/// </summary>
	[Space] public bool ToggleCrouch = true;

	//Unity callback.
	private void Awake()
    {
        CharacterController = GetComponent<CharacterController>();
        FPSController = GetComponent<FirstPersonController>();

        if (FPSController == null)
        {
            Debug.Log("CrouchController.cs: m_fpsController could not get standard asset FPS Controller component.");
        }

        standingHeight = CharacterController.height;
        crouchHeight = standingHeight / 2;
    }

	//Unity callback.
    private void Update()
    {
		CrouchLogic();
    }

	/// <summary>
	/// A method for the crouch input logic.
	/// </summary>
    private void CrouchLogic ()
    {
		if (FPSController.IsRunningVariable.Value)
		{
			isCrouching = false;

			Stand();

			return;
		}	

		if (ToggleCrouch)
		{
			if (Input.GetButtonDown("Crouch"))
			{
				isCrouching = !isCrouching;
			}	
		}
		else
		{
			if (Input.GetButton("Crouch"))
			{
				isCrouching = true;
			}
			else
			{
				isCrouching = false;
			}
		}

		if (isCrouching)
		{
			Crouch();
		}
		else
		{
			Stand();
		}
	}

	/// <summary>
	/// Sets the player state to stand.
	/// </summary>
    private void Stand()
    {
        //return the player's walking speed to normal to account for Standing status
        FPSController.WalkSpeed = FPSController.InitialWalkSpeed;

        CharacterController.height = Mathf.Lerp(CharacterController.height,
			standingHeight,
			Time.deltaTime * CrouchTransitionSpeed);

        transform.position = new Vector3(transform.position.x,
			Mathf.Lerp(CharacterController.height / 2,
					standingHeight / 2,
					Time.deltaTime * CrouchTransitionSpeed),
			transform.position.z);
    }

	/// <summary>
	/// Sets the player state to crouch.
	/// </summary>
    private void Crouch()
    {
        //adjust speed of walking to account for crouching status
        FPSController.WalkSpeed = FPSController.InitialWalkSpeed / 2.0f;

        CharacterController.height = Mathf.Lerp(CharacterController.height,
			crouchHeight,
			Time.deltaTime * CrouchTransitionSpeed);

        transform.position = new Vector3(transform.position.x,
			Mathf.Lerp(CharacterController.height / 2,
					crouchHeight / 2,
					Time.deltaTime * CrouchTransitionSpeed),
			transform.position.z);
    }

}
