﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class controls the initialization of the game, both editor and application.
/// </summary>
public class GameInitialize : MonoBehaviour {

	/// <summary>
	/// The current active initialize settings.
	/// </summary>
	[Tooltip("The current active initialize settings.")] public GameInitializeSettings ActiveSettings;

	//Unity callback.
	private void Awake()
	{
		if (ActiveSettings == null)
		{
			Debug.LogError("Error while initializing! Initialize settings do not exist!");

			//We don't want to keep playing without the correct initialization.
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
			Application.Quit();
#endif

			return;
		}

		ApplySettings();
	}

	private void ApplySettings ()
	{
		Application.targetFrameRate = ActiveSettings.TargetApplicationFPS;

#if UNITY_EDITOR
		Debug.unityLogger.logEnabled = ActiveSettings.LogDebugs;
#elif UNITY_STANDALONE
		Debug.unityLogger.logEnabled = ActiveSettings.LogDebugs;
#endif

		//We only need this object to set our initial settings.
		Destroy(gameObject);
	}
}
