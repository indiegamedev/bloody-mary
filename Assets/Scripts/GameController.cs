﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is being attached to the GameController object, this is used to gather the gameobject from a static non-monobehaviour script.
/// </summary>
public class GameController : MonoBehaviour {

	//No logic needed.
}
