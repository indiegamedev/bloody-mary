﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.Characters.FirstPerson;

/// <summary>
/// This class controls the player's in-game actions.
/// </summary>
public class PlayerActionLogic : MonoBehaviour {

	/// <summary>
	/// The player manager component.
	/// </summary>
	public PlayerManager PlayerManager
	{
		get; private set;
	}

	/// <summary>
	/// The FPS controller component.
	/// </summary>
	public FirstPersonController FirstPersonController
	{
		get; private set;
	}

	/// <summary>
	/// The inventory manager component.
	/// </summary>
	public InventoryManager InventoryManager
	{
		get; private set;
	}

	/// <summary>
	/// The player camera transform.
	/// </summary>
	public Transform PlayerView;

	/// <summary>
	/// The UpdateMoney Event.
	/// </summary>
	[Space(5)] [Header("COLLECTABLE && TRANSPORT SYSTEM")] [Space(5)] public GameEvent UpdateMoney;

	/// <summary>
	/// The money variable scrp. object.
	/// </summary>
	[Space] public FloatVariable MoneyVariable;

	/// <summary>
	/// Where the player holds an object in front of them.
	/// </summary>
	[Space(5)] public Transform ObjectHoldPosition;

	[Space()] public AudioClip ItemPickupSound;
	public AudioClip ItemPlaceSound;
	public AudioClip ItemDropSound;
	public AudioClip ItemTakeSound;

	/// <summary>
	/// The maximum distance you can interact objects within.
	/// </summary>
	[Space(5)] [Header("INTERACTION")] [Space(5)] [Tooltip("Maximum distance from which the player can interact with objects.")] [Range(0f, 20f)] public float MaxInteractionDistance = 3f;

	/// <summary>
	/// The look angle limit (X angle) in degrees to enable inventory.
	/// </summary>
	[Space] [Header("INVENTORY")] [Space(5)] public float InventoryEnableLookLimit = 0.5f;

	[Space()]

	public AudioClip InventoryOpenSound;
	public AudioClip InventoryCloseSound;

	/// <summary>
	/// Last selected slot in inventory.
	/// </summary>
	private GameObject lastSelectedSlot;

	/// <summary>
	/// The original walk speed.
	/// </summary>
	//private float originalWalkSpeed;

	/// <summary>
	/// Stores what the target raycast returns.
	/// </summary>
	private GameObject target;

	private GameObject heldObject;  // The object that the player grabbed
	private GameObject placeArea;   // GameObject representing an area the player can place an object
	private Rigidbody heldObjectRigidbody;  // Rigidbody of the object the player is holding
	private Collider heldObjectCollider;   // The collider of the object the player is holding
	private bool isCarrying = false;   // Bool determing if the player is holding an object or not
	

	//Unity callback.
	private void Awake()
	{
		this.GetInstances();

		this.Initialize();
	}

	private void Start()
	{
		InventoryManager = AutoMonoBehaviour<InventoryManager>.Instance;
	}

	/// <summary>
	/// Gets the component instances.
	/// </summary>
	private void GetInstances ()
	{
		PlayerManager = AutoMonoBehaviour<PlayerManager>.Instance;
		FirstPersonController = GetComponent<FirstPersonController>();
	}

	/// <summary>
	/// Initializes the actions.
	/// </summary>
	private void Initialize ()
	{
		//originalWalkSpeed = FirstPersonController.WalkSpeed;

		UIPlayer.Instance.SetInventoryState(true);
	}

	//Unity callback.
	private void Update()
	{
		this.LookLogic();
		this.TargetCastLogic();
		this.SetPlayerTarget();
		this.PlaceObjectLogic();
		this.PickupObjectLogic();
	}


	//Unity callback.
	private void FixedUpdate()
	{
		this.CarryObjectLogic();
	}

	/// <summary>
	/// Logic for target raycasting.
	/// </summary>
	private void TargetCastLogic()
	{
		RaycastHit _hit;

		if (Physics.Raycast(PlayerView.position, PlayerView.TransformDirection(Vector3.forward), out _hit, MaxInteractionDistance))
		{
			if (_hit.collider.gameObject.CompareTag("Item"))
			{
				UIPlayer.Instance.UpdateItemPickupText(_hit.collider.gameObject);
			}
			else
			{
				UIPlayer.Instance.UpdateItemPickupText(null, true);
			}

			target = _hit.collider.gameObject;
		}
		else
		{
			target = null;

			UIPlayer.Instance.UpdateItemPickupText(null, true);
		}
	}

	/// <summary>
	/// Logic for the LookStraightUp mechanic.
	/// </summary>
	public void LookLogic ()
	{
		// If player looks up to a certain degree, activate LookStraigtUp canvas and functionality
		if (PlayerView.localRotation.x < -InventoryEnableLookLimit)
		{
			//originalWalkSpeed = FirstPersonController.WalkSpeed;
			// Disable movement while LookStraightUp is active, movement keys will now navigate the inventory
			FirstPersonController.WalkSpeed = 0f;

			// If LookStraightUp canvas is inactive...
			if (!UIPlayer.Instance.Components.InventoryPanel.activeSelf)
			{
				// Activate it and set focus to the default button (top left slot)
				EventSystem.current.SetSelectedGameObject(InventoryManager.InventorySlotObjects[0]);
				UIPlayer.Instance.SetInventoryState(true);

				/*GameSoundManager.PlaySoundSettings settings = new GameSoundManager.PlaySoundSettings(
					AudioPlayType.SFX, InventoryOpenSound, false, 0.3f, 1f, transform.position,
					new GameSoundManager.Sound3DSettings(0f, Vector2.zero, 0f, 0));

				GameSoundManager.Instance.PlaySound(settings);*/
			}

			// If inventory loses focus (happens when mouse is clicked), reset focus to the las slot that had focus
			if (EventSystem.current.currentSelectedGameObject == null)
			{
				EventSystem.current.SetSelectedGameObject(lastSelectedSlot);
			}
			else
			{
				// Store the last slot to have focus so if focus is lost, it can be reset
				lastSelectedSlot = EventSystem.current.currentSelectedGameObject;
			}
		}
		// If player view isn't high enough to activate LookStraightUp...
		else
		{
			if (UIPlayer.Instance.Components.InventoryPanel.activeSelf)
			{
				/*GameSoundManager.PlaySoundSettings settings = new GameSoundManager.PlaySoundSettings(
					AudioPlayType.SFX, InventoryCloseSound, false, 0.3f, 1f, transform.position,
					new GameSoundManager.Sound3DSettings(0f, Vector2.zero, 0f, 0));

				GameSoundManager.Instance.PlaySound(settings);*/

				// Deactivate LookStraightUp canvas and allow movement again
				UIPlayer.Instance.SetInventoryState(false);
			}
		}
	}

	/// <summary>
	/// Logic for object pickup.
	/// </summary>
	private void PickupObjectLogic ()
	{
		// If the player presses the "grab/drop" key
		if (Input.GetKeyUp(KeyCode.F))
		{
			// And if the player is not current holding something and what they're looking at can be carried...
			if (PlayerManager.PlayerTarget != null && !isCarrying && PlayerManager.PlayerTarget.CompareTag("Item"))
			{
				CarryObject();
			}
		}
	}

	/// <summary>
	/// Logic for placing objects.
	/// </summary>
	private void PlaceObjectLogic ()
	{
		// If the player presses the "grab/drop" key...
		if (Input.GetKeyUp(KeyCode.F))
		{
			// And if the player is looking at an acceptable placing area and is currently carrying something...
			if (PlayerManager.PlayerTarget != null && isCarrying && heldObject != null && PlayerManager.PlayerTarget.CompareTag("PlaceArea"))
			{
				placeArea = PlayerManager.PlayerTarget.gameObject;                            // Stores a reference to the placing area
				heldObject.transform.position = new Vector3(placeArea.transform.position.x,
															placeArea.transform.position.y + 2f,
															placeArea.transform.position.z);        // Sets the carried object's position on the placing area (actual "placing" of the object)
				isCarrying = false;                                                                 // Sets isCarrying bool to false
				heldObjectRigidbody.useGravity = true;                                              // Reenable object's gravity
				heldObjectCollider.enabled = true;                                                  // Reenable object's collider

				Debug.Log("You placed " + heldObject.gameObject.name + " on" + placeArea.gameObject.name);

				/*GameSoundManager.PlaySoundSettings settings = new GameSoundManager.PlaySoundSettings(
					AudioPlayType.SFX, ItemPlaceSound, false, 0.5f, 1f, heldObject.transform.position,
					new GameSoundManager.Sound3DSettings(1f, new Vector2(1f, 3f), 1f, 180));

				GameSoundManager.Instance.PlaySound(settings);*/

				heldObject = null;                                                                  // Set reference to held object to null
				UIPlayer.Instance.UpdateItemPickupInfoText("", true);                               // Set display text to empty
			}
			else
			{
				DropObject();
			}
		}
	}

	/// <summary>
	/// Logic while carrying a object.
	/// </summary>
	private void CarryObjectLogic()
	{
		if (isCarrying)
		{
			// Text letting the player know their options when carrying an object
			UIPlayer.Instance.UpdateItemPickupInfoText("You are carrying " + heldObject.name + ". \nPress Q to rotate item left. \nPress E to rotate item right. \nPress R to put item in your backpack. \nPress F anywhere to drop item. \nPress F while looking at placing area to place item");

			// If the player presses Q...
			if (Input.GetKey(KeyCode.Q))
			{
				// Rotate the object to the left
				heldObject.transform.RotateAround(ObjectHoldPosition.transform.position, Vector3.up, 2);
			}
			// If the player presses E...
			else if (Input.GetKey(KeyCode.E))
			{
				// Rotate the object to the right
				heldObject.transform.RotateAround(ObjectHoldPosition.transform.position, Vector3.up, -2);
			}
			// If the player presses R...
			else if (Input.GetKey(KeyCode.R))
			{
				// Store item in inventory

				/*GameSoundManager.PlaySoundSettings settings = new GameSoundManager.PlaySoundSettings(
					AudioPlayType.SFX, ItemTakeSound, false, 0.5f, 1f, transform.position,
					new GameSoundManager.Sound3DSettings(0f, Vector2.zero, 1f, 0));

				GameSoundManager.Instance.PlaySound(settings);*/

				KeepItem();
				return;
			}

			// If carried object isn't in the proper "carry" position in front of the player (is moving or looking around)...
			if (Vector3.Distance(ObjectHoldPosition.transform.position, heldObject.transform.position) > 0.001f && isCarrying)
			{
				// Reset held object's local rotation
				heldObject.transform.localRotation = Quaternion.Lerp(heldObject.transform.localRotation, Quaternion.identity, 20f * Time.deltaTime);

				// Sets carried object's position to predetermined position in front of the player (The actual "carrying" of the object)
				heldObject.transform.Translate((ObjectHoldPosition.transform.position - heldObject.transform.position) * 20f * Time.deltaTime);
			}
		}
	}

	/// <summary>
	/// Sets what the player is looking at (target) and injects it into the PlayerTargetManager's playertarget variable.
	/// </summary>
	private void SetPlayerTarget()
	{
		if (target != null)
		{
			PlayerManager.PlayerTarget = target.gameObject;
		}
		else
		{
			PlayerManager.PlayerTarget = null;
		}
	}

	/// <summary>
	/// Starts the carry process for items / objects.
	/// </summary>
	private void CarryObject ()
	{
		heldObject = PlayerManager.PlayerTarget.gameObject;     // Stores what the player is looking at as the object they're carrying
		heldObjectRigidbody = heldObject.GetComponent<Rigidbody>();   // Store the object's rigidbody
		heldObjectCollider = heldObject.GetComponent<Collider>();     // Store the object's collider
		heldObjectRigidbody.useGravity = false;                       // Turn off object's gravity
		heldObjectCollider.enabled = false;                           // Disable the object's collider

		Debug.Log("You are carrying " + heldObject.gameObject.name);
		isCarrying = true;                                            // The player is carrying an object

		/*GameSoundManager.PlaySoundSettings settings = new GameSoundManager.PlaySoundSettings(
			AudioPlayType.SFX, ItemPickupSound, false, 0.35f, 1f, heldObject.transform.position,
			new GameSoundManager.Sound3DSettings(1f, new Vector2(1f, 3f), 1f, 180));

		GameSoundManager.Instance.PlaySound(settings);*/
	}

	/// <summary>
	/// Drops the current object.
	/// </summary>
	private void DropObject()
	{
		// If player is carrying an object...
		if (isCarrying && heldObject != null)
		{
			isCarrying = false; // Set isCarrying bool to false
			heldObjectRigidbody.useGravity = true; // Turn on gravity for object
			heldObjectCollider.enabled = true; // enable object's collider

			Debug.Log("You dropped " + heldObject.gameObject.name + " on the ground");

			/*GameSoundManager.PlaySoundSettings settings = new GameSoundManager.PlaySoundSettings(
					AudioPlayType.SFX, ItemDropSound, false, 0.35f, 1f, heldObject.transform.position,
					new GameSoundManager.Sound3DSettings(1f, new Vector2(1f, 3f), 1f, 180));

			GameSoundManager.Instance.PlaySound(settings);*/

			heldObject = null; // Set reference to carried object to null
			UIPlayer.Instance.UpdateItemPickupInfoText("", true); // Set display text to empty
		}
	}

	/// <summary>
	/// Equips an item to inventory.
	/// </summary>
	private void KeepItem()
	{
		InventoryManager.Add(heldObject.gameObject.GetComponent<ItemData>().Item); // Add item to inventory

		Destroy(heldObject); // Destory Game Object

		heldObject = null; // Reset reference to carried object to null
		isCarrying = false; // Set isCarrying bool to false

		UIPlayer.Instance.UpdateItemPickupInfoText("", true); // Set display text to empty
	}
}
