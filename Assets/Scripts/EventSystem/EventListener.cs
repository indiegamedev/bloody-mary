﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventListener : MonoBehaviour
{
    // List of Events and responses
    public List<EventAndResponse> EventAndResponses = new List<EventAndResponse>();

    private void OnEnable()
    {
        if (EventAndResponses.Count >= 1)
        {
            foreach (EventAndResponse eAndR in EventAndResponses)
            {
                eAndR.GameEvent.Register(this);
            }
        }
    }

    private void OnDisable()
    {
        if (EventAndResponses.Count >= 1)
        {
            foreach (EventAndResponse eAndR in EventAndResponses)
            {
                eAndR.GameEvent.DeRegister(this);
            }
        }
    }

    [ContextMenu("Raise Events")]
    public void OnEventRaised(GameEvent passedEvent)
    {

        for (int i = EventAndResponses.Count - 1; i >= 0; i--)
        {
            // Check if the passed event is the correct one
            if (passedEvent == EventAndResponses[i].GameEvent)
            {
                // Uncomment the line below for debugging the event listens and other details
                //Debug.Log("Called Event named: " + eventAndResponses[i].name + " on GameObject: " + gameObject.name);


                EventAndResponses[i].EventRaised();
            }
        }

    }
}


[System.Serializable]
public class EventAndResponse
{
    public string Name;
    public GameEvent GameEvent;
    public UnityEvent Response;
    public ResponseWithString ResponseForSentString;
    public ResponseWithInt ResponseForSentInt;
    public ResponseWithFloat ResponseForSentFloat;
    public ResponseWithBool ResponseForSentBool;

    public void EventRaised()
    {
        // default/generic
        if (Response.GetPersistentEventCount() >= 1) // always check if at least 1 object is listening for the event
        {
            Response.Invoke();
        }

        // string
        if (ResponseForSentString.GetPersistentEventCount() >= 1)
        {
            ResponseForSentString.Invoke(GameEvent.SentString);
        }

        // int
        if (ResponseForSentInt.GetPersistentEventCount() >= 1)
        {
            ResponseForSentInt.Invoke(GameEvent.SentInt);
        }

        // float
        if (ResponseForSentFloat.GetPersistentEventCount() >= 1)
        {
            ResponseForSentFloat.Invoke(GameEvent.SentFloat);
        }

        // bool
        if (ResponseForSentBool.GetPersistentEventCount() >= 1)
        {
            ResponseForSentBool.Invoke(GameEvent.SentBool);
        }

    }
}

[System.Serializable]
public class ResponseWithString : UnityEvent<string>
{
}

[System.Serializable]
public class ResponseWithInt : UnityEvent<int>
{
}

[System.Serializable]
public class ResponseWithFloat : UnityEvent<float>
{
}

[System.Serializable]
public class ResponseWithBool : UnityEvent<bool>
{
}