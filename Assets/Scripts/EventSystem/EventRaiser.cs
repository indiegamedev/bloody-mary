﻿using UnityEngine;

// Optional class to raise an event manually
public class EventRaiser : MonoBehaviour
{
    public GameEvent EventToRaise;

    public void RaiseEvent()
    {
        if (EventToRaise == null)
        {
            Debug.Log("Event was not set for Event Raiser on GameObject named:" + gameObject.name);
            return;
        }

        EventToRaise.Raise();
    }
}
