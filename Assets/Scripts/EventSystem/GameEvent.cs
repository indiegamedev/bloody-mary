﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameEvent", fileName = "NewGameEvent")]
public class GameEvent : ScriptableObject
{
    // Optional data types
    public string SentString;
    public int    SentInt;
    public float  SentFloat;
    public bool   SentBool;

    private List<EventListener> eventListeners = new List<EventListener>();

    public void Raise()
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
        {
            eventListeners[i].OnEventRaised(this);
        }
    }

    public void Register(EventListener passedEvent)
    {
        if (!eventListeners.Contains(passedEvent))
        {
            eventListeners.Add(passedEvent);
        }
    }

    public void DeRegister(EventListener passedEvent)
    {
        if (eventListeners.Contains(passedEvent))
        {
            eventListeners.Remove(passedEvent);
        }
    }
}