﻿using UnityEngine;

// Optional class to raise an event in Start method, useful for if you want to raise an event on something when it's Instantiated
public class EventRaiserOnStart : MonoBehaviour
{
    public GameEvent EventToRaise;

    private void Start()
    {
        RaiseEvent();
    }

    public void RaiseEvent()
    {
        if (EventToRaise == null)
        {
            Debug.Log("Event was not set for Event Raiser on GameObject named:" + gameObject.name);
            return;
        }

        EventToRaise.Raise();
    }
}
