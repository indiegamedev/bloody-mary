﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

/// <summary>
/// This class is a base for the player object. Contains all components within the player object. Executed before other player scripts.
/// </summary>
public class PlayerBase : MonoBehaviour {

	/// <summary>
	/// The Player Action component.
	/// </summary>
	public PlayerActionLogic PlayerActionLogic
	{
		get; private set;
	}

	/// <summary>
	/// The FPS Controller component.
	/// </summary>
	public FirstPersonController FirstPersonController
	{
		get; private set;
	}

	/// <summary>
	/// The Crouch Controller component.
	/// </summary>
	public CrouchController CrouchController
	{
		get; private set;
	}

	private void Awake()
	{
		this.PlayerActionLogic = GetComponent<PlayerActionLogic>();
		this.FirstPersonController = GetComponent<FirstPersonController>();
		this.CrouchController = GetComponent<CrouchController>();
	}
}
