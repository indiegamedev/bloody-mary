﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to create/get an instance of a component. The component needs to exist in the current scene.
/// After this has been called for a specific component, the system will cache the particular object so we don't need to search for it again.
/// Still, it is not recommended to call this rapidly e.g. in Update.
/// </summary>
/// <typeparam name="T">The type of the component.</typeparam>
public class AutoMonoBehaviour<T> : MonoBehaviour where T : Component {

	public static GameObject ThisObject;

	private static T _instance;

	public static T Instance
	{
		get
		{
			if (ThisObject == null)
			{
				ThisObject = FindObjectOfType<GameController>().gameObject;
			}

			if (_instance == null)
			{
				var com = ThisObject.GetComponent<T>();

				//We'll manually search the object.
				if (com == null)
				{
					com = FindObjectOfType<T>();
				}

				_instance = com;
			}		

			return _instance;
		}
	}
}
