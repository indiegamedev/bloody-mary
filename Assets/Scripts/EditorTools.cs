﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

/// <summary>
/// Custom tools for the editor UI.
/// </summary>
public class EditorTools {

	[MenuItem("Tools/Scenes/Open Prototype Scene")]
	static void LoadPrototypeScene ()
	{
		EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
		EditorSceneManager.OpenScene("Assets/Scenes/SampleScene.unity");
	}
}
