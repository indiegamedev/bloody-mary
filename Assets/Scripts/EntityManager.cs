﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to manage entities and their spawn behaviour. This class does not control the AI behaviour of the entities.
/// </summary>
public class EntityManager : MonoBehaviour
{
	/// <summary>
	/// Bool determining if the enemy is currently spawned.
	/// </summary>
	public bool IsCurrentlySpawned
	{
		get; private set;
	}

	/// <summary>
	/// Is the spawn system currently active?
	/// </summary>
	public bool IsActive = false;

	/// <summary>
	/// The entity that is going to be spawned.
	/// </summary>
    [Space] public GameObject EntityToSpawn;

	/// <summary>
	/// The minimum spawn interval.
	/// </summary>
	[Space(4f)] [SerializeField] private float MinimumSpawnInterval;

	/// <summary>
	/// The maximum spawn interval.
	/// </summary>
	[SerializeField] private float MaximumSpawnInterval; 

	private float spawnInterval;     // Current spawn interval
    private float spawnCountdown;    // Timer for spawn interval, counts down to 0
    private SpawnPoint[] spawnPoints; // An array of all the hand-placed spawn points

	//Unity callback.
    private void Awake()
    {
		this.CacheSpawnPoints();

		this.ResetSpawnCountdown();
    }

	//Unity callback.
    private void Update()
    {
		if (!IsActive)
			return;

        this.RunEntityLogic();
    }

	/// <summary>
	/// Caches the spawn points to an array.
	/// </summary>
	private void CacheSpawnPoints ()
	{
		spawnPoints = GameObject.FindObjectsOfType<SpawnPoint>();
	}

	/// <summary>
	///  Method handling the logic spawn interval logic of an entity.
	/// </summary>
	private void RunEntityLogic()
    {
        if (!IsCurrentlySpawned)  // If an enemy is not currently spawned...
        {
            spawnCountdown -= Time.deltaTime; // Then start the countdown from the randomly determined spawn interval

            if (spawnCountdown <= 0f)  // Once the spawn countdown reaches 0...
            {
				SpawnPoint point = this.GetRandomSpawnPoint();

				this.SpawnEntity(point);
            }
        }
    }

	/// <summary>
	/// Spawns an entity and assigns it to a random spawn point.
	/// </summary>
	/// <param name="point"></param>
	public void SpawnEntity (SpawnPoint point)
	{
		if (point == null)
		{
			throw new Exception("Cannot spawn entity '" + EntityToSpawn.name + "' without spawnpoint!");
		}

		// Spawn the enemy at a spawnpoint that's randomly selected from the array of spawn points
		Instantiate(EntityToSpawn, point.Position, point.Rotation);

		IsCurrentlySpawned = true;

		//Reset the countdown to zero.
		this.ResetSpawnCountdown();
	}

	/// <summary>
	/// Method to randonly select a spawn interval between the min and max spawn interval fields, then reset the countdown to that.
	/// </summary>
	private void ResetSpawnCountdown()
    {
        spawnInterval = UnityEngine.Random.Range(MinimumSpawnInterval, MaximumSpawnInterval);
        spawnCountdown = spawnInterval;
    }

	/// <summary>
	/// Gets a random usable spawn point.
	/// </summary>
	/// <returns>Spawn point.</returns>
	public SpawnPoint GetRandomSpawnPoint ()
	{
		List<SpawnPoint> allowedPoints = new List<SpawnPoint>();

		foreach (SpawnPoint point in spawnPoints)
		{
			if (!point.IsUsable)
				continue;

			allowedPoints.Add(point);
		}

		return allowedPoints[UnityEngine.Random.Range(0, allowedPoints.Count - 1)];
	}
}
