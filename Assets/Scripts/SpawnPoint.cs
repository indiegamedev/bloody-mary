﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

/// <summary>
/// A component for a entity spawn point. See <see cref="EntityManager"/> for the actual entity logic.
/// </summary>
public class SpawnPoint : MonoBehaviour {

	/// <summary>
	/// Can this spawn point be used?
	/// </summary>
	[Header("SPAWN POINT SETTINGS")] [Space(6)] [Tooltip("Can this spawn point be used?")] public bool IsUsable = true;

	/// <summary>
	/// Can we show the debug gizmos in the editor?
	/// </summary>
	[Space(2)] [Header("DEBUG SETTINGS")] [Space(6)] [Tooltip("Can we show the debug gizmos in the editor?")] public bool DebugEditor = true;

	/// <summary>
	/// The color of the debug block.
	/// </summary>
	[Tooltip("The color of the debug block.")] public Color DebugColor;

	/// <summary>
	/// The deactivated color of the debug block.
	/// </summary>
	[Tooltip("The deactivated color of the debug block.")] public Color DebugColorDeactivated;

	/// <summary>
	/// The size of the debug block.
	/// </summary>
	[Space] [Tooltip("The size of the debug block.")] public Vector3 DebugBlockSize;

	/// <summary>
	/// Position of this point.
	/// </summary>
	public Vector3 Position
	{
		get
		{
			return transform.position;
		}
	}

	/// <summary>
	/// Rotation of this point (World).
	/// </summary>
	public Quaternion Rotation
	{
		get
		{
			return transform.rotation;
		}
	}

	public void OnDrawGizmos()
	{
		if (!DebugEditor)
			return;

		Gizmos.color = IsUsable == true ? DebugColor : DebugColorDeactivated;
		Gizmos.DrawCube(transform.position, DebugBlockSize);
	}
}
