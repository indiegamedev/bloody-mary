﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to manage the active player UI. UI elements (text, sliders, panels...) can be gathered from <see cref="UIPlayerComponents"/>, which will be initialized in this class.
/// </summary>
public class UIPlayer : MonoBehaviour {

	/// <summary>
	/// The UI component class, can be used to access all elements of the active Player UI.
	/// </summary>
	public UIPlayerComponents Components
	{
		get; private set;
	}

	/// <summary>
	/// The UI player data class, contains information about current player statistics like BPM and money.
	/// </summary>
	[Tooltip("The UI player data class, contains information about current player statistics like BPM and money.")] public UIPlayerData PlayerData;

	/// <summary>
	/// The static instance of UIPlayer.
	/// </summary>
	public static UIPlayer Instance;

	//Unity callback.
	private void Awake()
	{
		Instance = this;

		try
		{
			UIPlayerComponents com = GetComponent<UIPlayerComponents>();

			//Alternative for null-check; if the class is null, this will trigger an expection.
			//int a = com.GetHashCode();

			Components = com;
		}
		catch (System.Exception e)
		{
			throw new System.Exception("Error while initializing UIPlayer! " + "Has UIPlayerComponents.cs been added to the UI gameobject? \n | " + e.Message);
		}	
	}

	/// <summary>
	/// Updates the BPM text.
	/// </summary>
	public void UpdateBPMText ()
	{
        Components.BPMText.text = PlayerData.BPMVariable.Value.ToString() + " BPM" ;
	}

	/// <summary>
	/// Updates the Money text.
	/// </summary>
	public void UpdateMoneyText()
	{
        Components.MoneyText.text = "Money: $" + PlayerData.MoneyVariable.Value.ToString("00.00");
	}

	/// <summary>
	/// Updates the item pickup text.
	/// </summary>
	/// <param name="currentItem">Current item gameobject.</param>
	/// /// <param name="isEmpty">Should the text be empty?</param>
	public void UpdateItemPickupText(GameObject currentItem, bool isEmpty = false)
	{
		Components.ItemPickupText.text = isEmpty == false ? ("You are looking at " + currentItem.name + ". Press F to pick it up.") : "";
	}

	/// <summary>
	/// Updates the item pickup info text.
	/// </summary>
	/// <param name="text">Current text.</param>
	/// /// <param name="isEmpty">Should the text be empty?</param>
	public void UpdateItemPickupInfoText(string text, bool isEmpty = false)
	{
		Components.ItemInformationText.text = isEmpty == false ? text : "";
	}


	/// <summary>
	/// Sets the inventory panel state.
	/// </summary>
	/// <param name="enabled">The state of the inventory panel.</param>
	public void SetInventoryState (bool enabled)
	{
		Components.InventoryPanel.SetActive(enabled);
	}

	/// <summary>
	/// This class contains information about the player data like current money, bpm.
	/// </summary>
	[System.Serializable] public class UIPlayerData
	{
		/// <summary>
		/// The player money variable.
		/// </summary>
		[Tooltip("The player money variable.")] public FloatVariable MoneyVariable;

		/// <summary>
		/// The player BPM variable.
		/// </summary>
		[Tooltip("The player BPM variable.")] public IntVariable BPMVariable;
	}
}
