﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// This class holds instances of each player UI element. <see cref="UIPlayer"/> is used to control these elements.
/// </summary>
public class UIPlayerComponents : MonoBehaviour {

	/// <summary>
	/// Text element for Money variable.
	/// </summary>
	[Tooltip("Text element for Money variable.")] public Text MoneyText;

	/// <summary>
	/// Text element for BPM variable.
	/// </summary>
	[Tooltip("Text element for BPM variable.")] public Text BPMText;


	/// <summary>
	/// Text element for item pickup notification.
	/// </summary>
	[Tooltip("Text element for item pickup notification.")] [Space] public Text ItemPickupText;

	/// <summary>
	/// Text element for item pickup information.
	/// </summary>
	[Tooltip("Text element for item pickup information.")] [Space] public Text ItemInformationText;

	/// <summary>
	/// GameObject element for inventory.
	/// </summary>
	[Tooltip("GameObject element for inventory.")] [Space] public GameObject InventoryPanel;

	/// <summary>
	/// The quest popup parent.
	/// </summary>
	[Space] public Transform UIQuestPopupParent;

	/// <summary>
	/// The complete quest text animation.
	/// </summary>
	[Space] public Animation CompleteQuestAnimation;

	/// <summary>
	/// The complete quest text.
	/// </summary>
	[Space] public TextMeshProUGUI CompleteQuestText;

	/// <summary>
	/// The complete quest reward text.
	/// </summary>
	public TextMeshProUGUI CompleteQuestRewardText;
}
