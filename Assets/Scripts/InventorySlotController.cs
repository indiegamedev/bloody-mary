﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is instantiated within a inventory UI slot element. This is used to change the properties of the slot.
/// </summary>
public class InventorySlotController : MonoBehaviour
{
    public Item Item;

    private void Start()
    {
        UpdateInfo();
    }

    public void UpdateInfo()
    {
        Text  _displayNameText   = transform.Find("Text").GetComponent<Text>();
        Image _displayIconImage  = transform.Find("Image").GetComponent<Image>();

        _displayNameText.text    = Item ? Item.itemName : "";
        _displayIconImage.sprite = Item ? Item.icon     : null;
        _displayIconImage.color  = Item ? Color.white   : Color.clear;
    }

    public void UseItem()
    {
        Item.Use();
    }
}
