﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class keeps objects alive during scene loads.
/// </summary>
public class DontDestroyOnLoad : MonoBehaviour {

	private void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}
}
