﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerChase : MonoBehaviour
{
    private NavMeshAgent chaser;
    private GameObject   player;

    private void Start()
    {
        chaser = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        chaser.destination = player.transform.position;
    }
}
