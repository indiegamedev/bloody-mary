﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the player and its attributes. 
/// </summary>
public class PlayerManager : MonoBehaviour {
	
	/// <summary>
	/// Current player in the scene.
	/// </summary>
	public PlayerBase Player
	{
		get; private set;
	}

	/// <summary>
	/// The prefab for the player.
	/// </summary>
	[Header("PLAYER")] [Space(5)] public GameObject GamePlayerPrefab;

	/// <summary>
	/// If true, it is not required to have the FPSController in the scene; this script will do it automatically.
	/// </summary>
	[Space(5)] [Tooltip("If true, it is not required to have the FPSController in the scene; this script will do it automatically.")] public bool UseAutomaticSpawnMethod = false;

	/// <summary>
	/// Initial spawn point for player.
	/// </summary>
	[Space] public Transform InitialPlayerSpawnPoint;

	/// <summary>
	/// The IsWalking variable object.
	/// </summary>
	[Space(5)] [Header("BPM SYSTEM")] [Space(5)] public BoolVariable IsWalkingVariable;

	/// <summary>
	/// The IsRunning variable object.
	/// </summary>
	public BoolVariable IsRunningVariable;

	/// <summary>
	/// The BPM variable object.
	/// </summary>
	[Space()] public IntVariable BPMVariable;

	/// <summary>
	/// The UpdateBPM game event.
	/// </summary>
	[Space()] public GameEvent UpdateBPMEvent;

	/// <summary>
	/// The update interval between the BPM calls.
	/// </summary>
	[Space(5)] [Range(0.001f, 10f)]  public float BPMEffectInterval = 1f;

	[Space(5)] [Header("TARGET SYSTEM")] [Space(5)] [HideInInspector] private GameObject playerTarget;

	/// <summary>
	/// Property to keep track of the player's current viewing target.
	/// </summary>
	public GameObject PlayerTarget
	{
		get
		{
			return playerTarget;
		}

		set
		{
			playerTarget = value;
		}
	}

	/// <summary>
	/// The interval timer.
	/// </summary>
	private float intervalTimer = 0f;

	//Unity callback.
	private void Awake()
	{
		BPMVariable.Value = 70;
	}

	//Unity callback.
	private void Start()
	{
		//this.SpawnPlayer(InitialPlayerSpawnPoint.position, InitialPlayerSpawnPoint.rotation);
	}

	//Unity callback.
	private void Update()
	{
		this.RaiseUpdate();
	}

	private void SpawnPlayer (Vector3 point, Quaternion rotation)
	{
		PlayerBase existingPlayer = FindObjectOfType<PlayerBase>();

		if (existingPlayer != null)
		{
			Debug.LogError("WARNING: A player object already exists in the scene. Consider switching PlayerManager components 'UseAutomaticSpawnMethod' to false in case you want to have the player in before playmode. Overriding system logic...");

			Player = existingPlayer;

			return;
		}

		this.Player = GameObject.Instantiate(GamePlayerPrefab, point, rotation).GetComponent<PlayerBase>();
	}

	private void RaiseUpdate ()
	{
		if (intervalTimer < BPMEffectInterval)
		{
			intervalTimer += Time.deltaTime;
		}
		//Call the action here.
		else if (intervalTimer >= BPMEffectInterval)
		{
			this.Call();
		}
	}

	/// <summary>
	/// Calls the BPM methods and resets the countdown.
	/// </summary>
	private void Call ()
	{
		this.BpmRestEffect();
		this.BpmWalkEffect();
		this.BpmRunEffect();

		intervalTimer = 0f;
	}

	/// <summary>
	/// BPM logic for player run state.
	/// </summary>
	private void BpmRunEffect()
	{
		// If player is running, increase BPM by 2 every second until it reaches 160 BPM
		if (IsRunningVariable.Value && !IsWalkingVariable.Value && BPMVariable.Value < 160)
		{
			BPMVariable.Value += 2;
		}
		// If player is running, and the BPM is greater than 160, increase BPM by 1 every second
		else if (IsRunningVariable.Value && !IsWalkingVariable.Value && BPMVariable.Value >= 160)
		{
			BPMVariable.Value += 1;
		}

		UpdateBPMEvent.Raise();
	}

	/// <summary>
	/// BPM logic for player walk state.
	/// </summary>
	private void BpmWalkEffect()
	{
		// If the player is walking, increase BPM by 1 every second until it reaches 100 which is the max BPM for simply walking
		if (IsWalkingVariable.Value && !IsRunningVariable.Value && BPMVariable.Value < 100)
		{
			BPMVariable.Value += 1;
		}
		// If the player is walking and BPM is over 100 (e.g. went from running to walking), then decrease BPM by 1 every second until it reaches 100
		else if (IsWalkingVariable.Value && !IsRunningVariable.Value && BPMVariable.Value > 100)
		{
			BPMVariable.Value -= 1;
		}

		UpdateBPMEvent.Raise();
	}

	/// <summary>
	/// BPM logic for player rest state.
	/// </summary>
	private void BpmRestEffect()
	{
		// If the player is at rest (not walking or running), then BPM will derease by 1 every second until it reaches the minimum of 70 BPM
		if (!IsWalkingVariable.Value && !IsRunningVariable.Value && BPMVariable.Value > 70)
		{
			BPMVariable.Value -= 1;
		}

		UpdateBPMEvent.Raise();
	}
}
