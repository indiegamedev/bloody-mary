using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
	/// <summary>
	/// This class is responsible for the player movement. 
	/// </summary>
    [RequireComponent(typeof(AudioSource))] [RequireComponent(typeof(CharacterController))] public class FirstPersonController : MonoBehaviour
    {
	    // Determining if the player's currently on a blackboard challenge (needed for the controls on the fps-controller)
	    public bool IsOnBlackboardChallenge { get; set; }
	    
        public BoolVariable IsRunningVariable;
        public BoolVariable IsWalkingVariable;

        [HideInInspector] public bool m_IsWalking;
        [SerializeField] private float m_WalkSpeed;
        [HideInInspector]
        public float WalkSpeed
        {
            get
            {
                return m_WalkSpeed;
            }

            set
            {
                m_WalkSpeed = value;
            }
        }

        private float m_initialWalkSpeed;
        [HideInInspector]
        public float InitialWalkSpeed
        {
            get
            {
                return m_initialWalkSpeed;
            }
            set
            {
                m_initialWalkSpeed = value;
            }
        }

        [SerializeField] private float m_RunSpeed;
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private float m_JumpSpeed;
        [SerializeField] private float m_StickToGroundForce;
        [SerializeField] private float m_GravityMultiplier;
        [SerializeField] private MouseLook m_MouseLook;
        [SerializeField] private bool m_UseFovKick;
		[SerializeField] [Space] private float m_MaxFov;
		[SerializeField] private float m_FovSpeed;
		[SerializeField] [Space] private bool m_UseHeadBob;
        [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
        [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
        [SerializeField] private float m_StepInterval;
        [SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
        [SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
        [SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.

        private Camera m_Camera;
        private bool m_Jump;
        private float m_YRotation;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;
        private Vector3 m_OriginalCameraPosition;
        private float m_StepCycle;
        private float m_NextStep;
        private bool m_Jumping;
        private AudioSource m_AudioSource;
		private float fpsCounter;
		private string prevFPS;
		private float defFOV;

        // Use this for initialization
        private void Start()
        {
            m_initialWalkSpeed = m_WalkSpeed;

            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
			defFOV = m_Camera.fieldOfView;
			m_OriginalCameraPosition = m_Camera.transform.localPosition;
            //m_FovKick.Setup(m_Camera);
            m_HeadBob.Setup(m_Camera, m_StepInterval);
            m_StepCycle = 0f;
            m_NextStep = m_StepCycle / 2f;
            m_Jumping = false;
            m_AudioSource = GetComponent<AudioSource>();
            m_MouseLook.Init(transform, m_Camera.transform);
        }


        // Update is called once per frame
        private void Update()
        {
            //RotateView();
            // the jump state needs to read here to make sure it is not missed
            if (!m_Jump)
            {
                //m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }

            if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
            {
                StartCoroutine(m_JumpBob.DoBobCycle());
                //PlayLandingSound();
                m_MoveDir.y = 0f;
                m_Jumping = false;
            }
            if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
            {
                m_MoveDir.y = 0f;
            }

            m_PreviouslyGrounded = m_CharacterController.isGrounded;

			this.FOVKick();
        }

        private void FOVKick ()
		{
			if (m_UseFovKick && m_CharacterController.velocity.magnitude > m_WalkSpeed + 0.1f)
			{
				if (!m_IsWalking)
				{
					m_Camera.fieldOfView = Mathf.Lerp(m_Camera.fieldOfView, m_MaxFov, (m_FovSpeed * 0.5f) * Time.deltaTime);
					return;
				}
			}

			m_Camera.fieldOfView = Mathf.Lerp(m_Camera.fieldOfView, defFOV, m_FovSpeed * Time.deltaTime);
		}

        private void PlayLandingSound()
        {
			/*GameSoundManager.PlaySoundSettings settings = new GameSoundManager.PlaySoundSettings(
				AudioPlayType.SFX, m_LandSound, false, 0.95f, 1f, new Vector3(transform.position.x, transform.position.y - 0.9f, transform.position.z),
					new GameSoundManager.Sound3DSettings(1f, new Vector2(0.2f, 2f), 1f, 180));

			GameSoundManager.Instance.PlaySound(settings);*/

			m_NextStep = m_StepCycle + .5f;
        }


        private void FixedUpdate()
        {
            RotateView();

            float speed;
            GetInput(out speed);
            // always move along the camera forward as it is the direction that it being aimed at
            Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                               m_CharacterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal);

            m_MoveDir.x = desiredMove.x * speed;
            m_MoveDir.z = desiredMove.z * speed;

            if (m_CharacterController.isGrounded)
            {
                m_MoveDir.y = -m_StickToGroundForce;

                if (m_Jump)
                {
                    m_MoveDir.y = m_JumpSpeed;
                    PlayJumpSound();
                    m_Jump = false;
                    m_Jumping = true;
                }
            }
            else
            {
                m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
            }
            m_CollisionFlags = m_CharacterController.Move(m_MoveDir * Time.fixedDeltaTime);

            ProgressStepCycle(speed);
            UpdateCameraPosition(speed);

            m_MouseLook.UpdateCursorLock();
        }


        private void PlayJumpSound()
        {
            m_AudioSource.clip = m_JumpSound;

			/*GameSoundManager.PlaySoundSettings settings = new GameSoundManager.PlaySoundSettings(
				AudioPlayType.SFX, m_JumpSound, false, 0.5f, 1f, new Vector3(transform.position.x, transform.position.y - 0.9f, transform.position.z),
					new GameSoundManager.Sound3DSettings(1f, new Vector2(0.2f, 2f), 1f, 180));

			GameSoundManager.Instance.PlaySound(settings);*/
        }


        private void ProgressStepCycle(float speed)
        {
            if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
            {
                m_StepCycle += (m_CharacterController.velocity.magnitude + (speed * (m_IsWalking ? 1f : m_RunstepLenghten))) *
                             Time.fixedDeltaTime;

                if (m_IsWalking)
                {
                    IsWalkingVariable.Value = true;
                    IsRunningVariable.Value = false;
                }
                else
                {
                    IsWalkingVariable.Value = false;
                    IsRunningVariable.Value = true;
                }
            }

            else
            {
                IsWalkingVariable.Value = false;
                IsRunningVariable.Value = false;
            }

            if (!(m_StepCycle > m_NextStep))
            {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            PlayFootStepAudio();
        }


        private void PlayFootStepAudio()
        {
            if (!m_CharacterController.isGrounded)
            {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];

            //randomize the pitch and volume
            // NOTE : pitch should be a bit higher than normal, given the player is a child
            // and therefore the steps wouldn't exactly be very hard

            //float pitch = Random.Range(1.8f, 2.2f);
            //float volume = m_IsWalking ? Random.Range(0.5f, 0.75f) : Random.Range(0.8f, 0.99f);

			/*GameSoundManager.PlaySoundSettings settings = new GameSoundManager.PlaySoundSettings(
				AudioPlayType.SFX, m_AudioSource.clip, false, volume, pitch, new Vector3(transform.position.x, transform.position.y - 0.9f, transform.position.z),
					new GameSoundManager.Sound3DSettings(1f, new Vector2(0.8f, 2f), 1f, 180));

			GameSoundManager.Instance.PlaySound(settings);*/

           // m_AudioSource.PlayOneShot(m_AudioSource.clip);


            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
        }


        private void UpdateCameraPosition(float speed)
        {
            Vector3 newCameraPosition = Vector3.zero;
            if (!m_UseHeadBob)
            {
                return;
            }
            if (m_CharacterController.velocity.magnitude > 1f && m_CharacterController.isGrounded)
            {
				m_Camera.transform.localPosition = Vector3.Lerp(m_Camera.transform.localPosition, 
					m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude +
									  (speed * (m_IsWalking ? 1f : m_RunstepLenghten))), Time.deltaTime * 12f);
                   ;
                newCameraPosition = Vector3.Lerp(newCameraPosition, m_Camera.transform.localPosition, Time.deltaTime * 12f);
                newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
            }
            else
            {
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
            }
			m_Camera.transform.localPosition = Vector3.Lerp(m_Camera.transform.localPosition, newCameraPosition, Time.deltaTime * 12f);
        }


        private void GetInput(out float speed)
        {
	        if (IsOnBlackboardChallenge)
	        {
		        speed = 0;
		        return; // Added this so there is no character movement while the blackboard challenge is active
			}
	        
            // Read input
            float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
            float vertical = CrossPlatformInputManager.GetAxis("Vertical");

            bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
            // On standalone builds, walk/run speed is modified by a key press.
            // keep track of whether or not the character is walking or running
            m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
            // set the desired speed to be walking or running
            speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
            m_Input = new Vector2(horizontal, vertical);

            // normalize input if it exceeds 1 in combined length:
            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }

            // handle speed change to give an fov kick
            // only if the player is going to a run, is running and the fovkick is to be used
            if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
            {
                StopAllCoroutines();
                //StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
            }
        }


        private void RotateView()
        {
            m_MouseLook.LookRotation(transform, m_Camera.transform);
        }


        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
        }

		private void OnGUI()
		{	
			int w = Screen.width, h = Screen.height;

			GUIStyle style = new GUIStyle();

			Rect rect = new Rect(3, 5, w, h * 2 / 100);
			Rect rect2 = new Rect(3, 25, w, h * 2 / 100);

			style.alignment = TextAnchor.UpperLeft;
			style.fontSize = h * 2 / 100;
			style.normal.textColor = Color.white;

			Vector3 velocity = new Vector3(m_MoveDir.x, 0, m_MoveDir.z);

			GUI.Label(rect, GetFrameRate(), style);

			string velo = velocity.magnitude.ToString("0.0") + " m/sec";

			GUI.Label(rect2, "Player Velocity: " + velo, style);
		}

		public string GetFrameRate()
		{
			if (fpsCounter < 0.5f)
			{
				fpsCounter += Time.deltaTime;
				return prevFPS;
			}

			fpsCounter = 0f;

			int num = Mathf.Max(Mathf.RoundToInt(Time.unscaledDeltaTime * 1000f), 1);

			string fps = string.Format("{0} FPS ({1}ms)", 1000 / num, num);

			prevFPS = fps;

			return fps;
		}
	}
}
